QT_DIR?=/usr/local/Qt-5.5.1
QT_BIN?=$(QT_DIR)/bin

QMAKE?=$(QT_BIN)/qmake
MOC?=$(QT_BIN)/moc
UIC?=$(QT_BIN)/uic

release: configure
	cd Core/ && $(MAKE) release && mv Intr ../Intr
	cd Shell/ && $(MAKE) && mv SIntr ../SIntr

check: release
	cd Core/ && ./IntrTests

configure:
	cd Shell/ && $(QMAKE) -makefile -o Makefile SIntr.pro

clean:
	rm -f SIntr Intr *~	
	cd Core/ && $(MAKE) -k clean
	cd Shell/ && $(MAKE) -k clean && rm -f Makefile
