#ifndef PROCESS_THREAD
#define PROCESS_THREAD

#include <QThread>
#include <QProcess>

namespace GUI_Thread {

class ProcessThread: public QThread
{
    Q_OBJECT

private:
    QProcess* m_proc;

    void run() Q_DECL_OVERRIDE;

public:
    ProcessThread(QProcess* proc):QThread(),m_proc(proc){}

    ProcessThread(const ProcessThread&) = delete;
    ProcessThread& operator=(const ProcessThread&) = delete;

signals:
    void ProcessThreadStarted();
    void ProcessThreadFailed();
};

} //namespace GUI_Thread

#endif // WORKERTHREAD

