#-------------------------------------------------
#
# Project created by QtCreator 2015-12-04T12:24:56
#
#-------------------------------------------------

QT       += core gui
QMAKE_CXXFLAGS += -std=c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SIntr
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    processthread.cpp \
    processoutputhandler.cpp

HEADERS  += mainwindow.h \
    processthread.h \
    processoutputhandler.h

FORMS    += mainwindow.ui
