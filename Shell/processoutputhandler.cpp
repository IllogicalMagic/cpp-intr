#include "processoutputhandler.h"

void GUI_Thread::ProcessOutputHandler::HandleOutput()
{
    m_dest->insertPlainText(m_proc->readAllStandardOutput());
}

void GUI_Thread::ProcessOutputHandler::HandleError()
{
    m_dest->insertPlainText(m_proc->readAllStandardError());
}

void GUI_Thread::ProcessOutputHandler::HandleMissingProgram()
{
    m_dest->insertPlainText("Program 'Intr' is missing\n");
}
