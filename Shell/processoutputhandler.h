#ifndef PROCESSOUTPUTHANDLER_H
#define PROCESSOUTPUTHANDLER_H

#include <QProcess>
#include <QTextBrowser>

namespace GUI_Thread {

class ProcessOutputHandler: public QObject
{
    Q_OBJECT

    QProcess* m_proc;
    QTextBrowser* m_dest;

    ProcessOutputHandler(const ProcessOutputHandler&) = delete;
    ProcessOutputHandler& operator=(const ProcessOutputHandler&) = delete;
public:
    ProcessOutputHandler(QProcess* proc, QTextBrowser* dest):
        m_proc(proc),m_dest(dest){}

public slots:
    void HandleOutput();
    void HandleError();
    void HandleMissingProgram();
};

} // namespace GUI_Thread

#endif // PROCESSOUTPUTHANDLER_H
