#include "processthread.h"

void GUI_Thread::ProcessThread::run()
{
    m_proc->start();
    if (m_proc->waitForStarted()) {
        emit ProcessThreadStarted();
        m_proc->waitForFinished(-1);
    }
    if (m_proc->error()==QProcess::FailedToStart)
        emit ProcessThreadFailed();
}
