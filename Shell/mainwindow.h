#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <processoutputhandler.h>
#include <processthread.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_RunButton_clicked();
    void on_StopButton_clicked();

signals:
    void StopProcess();

private:
    Ui::MainWindow *ui;
    void showOutput();
    void make_Process_connections(QProcess*,GUI_Thread::ProcessOutputHandler*,GUI_Thread::ProcessThread*);
    bool is_intr_args_valid();
};

#endif // MAINWINDOW_H
