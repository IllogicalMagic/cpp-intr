#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "processoutputhandler.h"
#include "processthread.h"

#include <cstdio>
#include <QTextStream>
#include <QTextBrowser>
#include <QProcess>
#include <QMainWindow>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->StopButton->hide();
    ui->StackSizeErrorLabel->hide();
    ui->FileNameErrorLabel->hide();

    ui->StackSizeEdit->setValidator(new QIntValidator(1,INT_MAX));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_RunButton_clicked()
{
    if (!is_intr_args_valid()) {
        ui->RunButton->show();
        ui->StopButton->hide();
        return;
    }

    QProcess* intr=new QProcess;
    intr->setProgram("./Intr");
    QStringList args;
    if (ui->WarningSupressionCheck->isChecked())
        args << "-noW";
    args << "-s" << ui->StackSizeEdit->text() << ui->FileNameEdit->text();
    intr->setArguments(args);
    GUI_Thread::ProcessOutputHandler* handler=
      new GUI_Thread::ProcessOutputHandler(intr,ui->OutputBox);
    GUI_Thread::ProcessThread* proc_thread=
      new GUI_Thread::ProcessThread(intr);

    make_Process_connections(intr,handler,proc_thread);

    proc_thread->start();
}

void MainWindow::on_StopButton_clicked()
{
    emit StopProcess();
}

void MainWindow::make_Process_connections
(QProcess* intr,
 GUI_Thread::ProcessOutputHandler* handler,
 GUI_Thread::ProcessThread* proc_thread)
{
    connect(proc_thread,&GUI_Thread::ProcessThread::ProcessThreadFailed,
            handler,&GUI_Thread::ProcessOutputHandler::HandleMissingProgram);

    connect(intr,&QProcess::readyReadStandardError,
	    handler,&GUI_Thread::ProcessOutputHandler::HandleError);
    connect(intr,&QProcess::readyReadStandardOutput,
	    handler,&GUI_Thread::ProcessOutputHandler::HandleOutput);

    connect(proc_thread,&GUI_Thread::ProcessThread::finished,
	    ui->RunButton,&QAbstractButton::show);
    connect(proc_thread,&GUI_Thread::ProcessThread::finished,
	    ui->StopButton,&QAbstractButton::hide);

    connect(intr,static_cast<void(QProcess::*)(int,QProcess::ExitStatus)>(&QProcess::finished),
            intr,&QObject::deleteLater);
    connect(intr,static_cast<void(QProcess::*)(int,QProcess::ExitStatus)>(&QProcess::finished),
            handler,&QObject::deleteLater);

    connect(proc_thread,&GUI_Thread::ProcessThread::finished,proc_thread,&QObject::deleteLater);
    connect(this,&MainWindow::StopProcess,intr,&QProcess::kill);
}

bool MainWindow::is_intr_args_valid()
{
    bool result=true;
    if (ui->StackSizeEdit->text().isEmpty()) {
        result=false;
        ui->StackSizeErrorLabel->show();
    }
    else ui->StackSizeErrorLabel->hide();

    if (ui->FileNameEdit->text().isEmpty()) {
        result=false;
        ui->FileNameErrorLabel->show();
    }
    else ui->FileNameErrorLabel->hide();

    return result;
}
