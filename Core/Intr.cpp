#include "Error.h"
#include "Table.h"
#include "SyntaxTree.h"
#include "TreeAnalyzer.h"
#include "FuncCont.h"
#include "Interpreter.h"

#include <cstdio>
#include <string>
#include <tuple>

#include "Parser/ParsingDriver.h"

const long unsigned default_stack_size=10000;

void wrong_param();
void parse_cmd_line(std::tuple<unsigned,bool,FILE*>&,
		    int argc,const char** argv);
void file_not_found_err(const char*);

int main(int argc,const char** argv)
{
  std::tuple<unsigned,bool,FILE*> cmd_args
    {default_stack_size,true,nullptr};
  parse_cmd_line(cmd_args,argc,argv);

  IntrError::ErrHandler err(std::get<1>(cmd_args));

  FILE* in=std::get<2>(cmd_args);
  unsigned stack_size=std::get<0>(cmd_args);
  yy::ParsingDriver pd(in,err);
  pd.Reset(in);
  if (pd.Parse())
    {
      FuncCont::FuncContainer* fc=pd.GetFC();
      Interpreting::Interpreter intr(*fc,err,stack_size);
      TreeAnalysis::TreeAnalyzer ta(fc,err,intr);
      if (ta.PrepareFuncFrames())
      	try
      	  {
      	    if (intr.Calculate())
      	      printf("Result is %lg\n",intr.GetResult());
      	  }
      	catch (std::bad_alloc)
      	  {
      	    err.Error("Out of memory");
      	  }
    }
  fclose(in);
  return err.Count();
}

void parse_cmd_line(std::tuple<unsigned,bool,FILE*>& res,
		    int argc,const char** argv)
{
  std::get<0>(res)=default_stack_size;

  if (argc<3) {
    bool warn=true;
    if (argc==2 && !strcmp("-noW",argv[1])) {
      warn=false;
      argc=1;
    }

    std::get<1>(res)=warn;
    FILE* in;
    if (argc==1) {
      in=fopen("expression.txt","r");
      if (!in)
        file_not_found_err("expression");
    }
    else {
      in=fopen(argv[argc-1],"r");
      if (!in)
	file_not_found_err(argv[argc-1]);
    }
    std::get<2>(res)=in;
    return;
  }
  if (argc>5)
    wrong_param();

  int i=1;
  while (i<argc) {
    if (!strcmp("-noW",argv[i])) {
      std::get<1>(res)=false;
    }

    else if (!strcmp("-s",argv[i])) {
      if (i+1==argc)
	wrong_param();
      ++i;
      std::string par=argv[i];
      try
	{
	  auto s_size=std::stoul(par);
	  if (s_size)
	    std::get<0>(res)=s_size;
	  else std::get<0>(res)=default_stack_size;
	}
      catch (std::out_of_range)
	{
	  fprintf(stderr,"Number is too big\n");
	  exit(1);
	}
      catch (std::invalid_argument)
	{
	  wrong_param();
	}
    }
    else if (i<argc-1) wrong_param();
    
    ++i;
  }
  FILE* in;
  if (i==argc)
    in=fopen("expression.txt","r");
  else in=fopen(argv[argc-1],"r");
  if (!in)
    file_not_found_err(argv[argc-1]);
  std::get<2>(res)=in;
}

void wrong_param()
{
  fprintf(stderr,"Usage: [-s %%u] [-noW] filename\n");
  exit(1);
}

void file_not_found_err(const char* f)
{
  fprintf(stderr,"'%s' is not found\n",f);
  exit(1);
}
